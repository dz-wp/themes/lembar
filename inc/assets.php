<?php declare( strict_types = 1 );

namespace Lembar\Assets;

use HM\Asset_Loader;
use Lembar\Data;

/**
 * Bootstrapper
 */
function bootstrap(): void {
	add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\enqueue' );
	add_filter( 'hm_asset_loader_dev_src', __NAMESPACE__ . '\\filter_dev_assets_src', 10, 2 );
}

/**
 * Enqueue JavaScript and CSS assets for the frontend
 *
 * If possible, this loads a version via the Webpack dev server,
 * otherwise it uses the built version of the file.
 */
function enqueue(): void {
	$bundles = [
		'runtime',
		'vendors~browser',
	];

	foreach ( $bundles as $name ) {
		Asset_Loader\enqueue(
			[
				'entry'       => $name,
				'handle'      => "lembar_{$name}",
				'dir_url'     => get_stylesheet_directory_uri() . '/things',
				'dir_path'    => dirname( __DIR__ ) . '/things',
				'has_css'     => false,
				'in_footer'   => true,
			],
		);
	}

	Asset_Loader\enqueue(
		[
			'entry'       => 'browser',
			'handle'      => 'lembar',
			'dir_url'     => get_stylesheet_directory_uri() . '/things',
			'dir_path'    => dirname( __DIR__ ) . '/things',
			'has_css'     => true,
			'in_footer'   => true,
			'script_data' => [
				'__lembar_data__' => Data\get_initial_data(),
			],
		],
	);
}

/**
 * Filter assets sources when running webpack dev server
 *
 * @param array<mixed> $sources       Asset sources.
 * @param array<mixed> $manifest_list Manifest list.
 *
 * @return array<mixed>
 */
function filter_dev_assets_src( array $sources, array $manifest_list ): array {
	$dir_url = trailingslashit( get_stylesheet_directory_uri() );

	foreach ( $sources as $key => $src_url ) {
		if ( $key === 'version' ) {
			continue;
		}

		$output_file = str_replace( $dir_url, '', $src_url );

		// phpcs:ignore SlevomatCodingStandard.ControlStructures.EarlyExit.EarlyExitNotUsed
		if ( ! empty( $manifest_list[ $output_file ] ) ) {
			$sources[ $key ] = $manifest_list[ $output_file ];
		}
	}

	return $sources;
}
