<?php // phpcs:disable SlevomatCodingStandard

/**
 * Plugin Name: HM PHP JS
 * Description: Execute JS code in PHP.
 * Author: Human Made Limited
 * Author URI: https://humanmade.com
 * Version: 1.0.0
 * License: GPLv2
 */

namespace HM\JS;

use V8Js;
use V8JsScriptException;

/**
 * Get stub script to setup server environment.
 *
 * @return string
 */
function get_stub() {
	$rest_url = get_rest_url();

	$stub = <<<END
// Set up browser-compatible APIs.
var window = this;
var console = {
	warn: print,
	error: print,
	log: ( print => it => print( JSON.stringify( it ) ) )( print )
};
// Expose more globals we might want.
var global = global || this;
var self = self || this;
var isSSR = true;
var restUrl = '$rest_url';
// Remove default top-level APIs.
delete exit;
delete var_dump;
delete require;
delete sleep;
END;

	return $stub;
}

function print_error_overlay_style() {
	?>
<style>.header,.preStyle{white-space:pre-wrap}body,html{overflow:hidden}.error-overlay{position:fixed;top:0;left:0;width:100%;height:100%;border:none;z-index:1000;--black:#293238;--dark-gray:#878e91;--red:#ce1126;--red-transparent:rgba(206, 17, 38, 0.05);--light-red:#fccfcf;--yellow:#fbf5b4;--yellow-transparent:rgba(251, 245, 180, 0.3);--white:#ffffff}.error-overlay .wrapper{width:100%;height:100%;box-sizing:border-box;text-align:center;background-color:var(--white)}.primaryErrorStyle{background-color:var(--light-red)}; .secondaryErrorStyle{background-color:var(--yellow)}.error-overlay .overlay{position:relative;display:inline-flex;flex-direction:column;height:100%;width:1024px;max-width:100%;overflow-x:hidden;overflow-y:auto;padding:.5rem;box-sizing:border-box;text-align:left;font-family:Consolas,Menlo,monospace;font-size:11px;line-height:1.5;color:var(--black)}.header{font-size:2em;font-family:sans-serif;color:var(--red);margin:0 2rem .75rem 0;flex:0 0 auto;max-height:50%;overflow:auto}.preStyle{display:block;padding:.5em;margin-top:.5em;margin-bottom:.5em;overflow-x:auto;border-radius:.25rem;background-color:var(--red-transparent);font-size:inherit}.codeStyle{font-family:Consolas,Menlo,monospace;font-size:inherit}.tab{color:rgba(255,255,255,.1)}.footer{font-family:sans-serif;color:var(--dark-gray);margin-top:.5rem;flex:0 0 auto}</style>
	<?php
}

/**
 * Render JS exception handler.
 *
 * @param V8JsScriptException $e Exception to handle.
 */
function handle_exception( V8JsScriptException $e ) {
	static $is_style_printed = false;

	if ( ! WP_DEBUG ) {
		trigger_error( 'SSR error: ' . $e->getMessage(), E_USER_WARNING ); // phpcs:ignore
		return;
	}

	if ( ! $is_style_printed ) {
		print_error_overlay_style();
		$is_style_printed = true;
	}

	$file = $e->getJsFileName();
	?>
	<div class="error-overlay"><div class="wrapper"><div class="overlay">
		<div class="header">Failed to render</div>
		<pre class="preStyle"><code class="codeStyle">
			<?php
			// phpcs:disable Squiz
			echo esc_html( $file ) . "\n";
			$trace = $e->getJsTrace();
			if ( $trace ) {
				$trace_lines = $error = explode( "\n", $e->getJsTrace() );
				echo esc_html( $trace_lines[0] ) . "\n\n";
			} else {
				echo $e->getMessage() . "\n\n";
			}
			// Replace tabs with tab character.
			$prefix = '> ' . (int) $e->getJsLineNumber() . ' | ';
			echo $prefix . str_replace(
				"\t",
				'<span class="tab">→</span>',
				esc_html( $e->getJsSourceLine() )
			) . "\n";
			echo str_repeat( " ", strlen( $prefix ) + $e->getJsStartColumn() );
			echo str_repeat( "^", $e->getJsEndColumn() - $e->getJsStartColumn() ) . "\n";
			// phpcs:enable Squiz
			?>
		</code></pre>
		<div class="footer">
			<p>This error occurred during server-side rendering and cannot be dismissed.</p>
			<?php if ( $file === 'ssrBootstrap' ) : ?>
				<p>This appears to be an internal error in SSR. Please report it on GitHub.</p>
			<?php elseif ( $file === 'ssrDataInjection' ) : ?>
				<p>This appears to be an error in your script's data. Check that your data is valid.</p>
			<?php endif ?>
		</div>
	</div></div></div>
	<?php
}

/**
 * Execute JavaScript
 *
 * @param  string   $script        Script to execute.
 * @param  string   $data          Optional data to add to the server env.
 * @param  callable $error_handler Optional function to call when error occures.
 *
 * @return mixed
 */
function exec_js( $script, $data = null, $error_handler = null ) {
	$v8 = new V8Js();
	$stub = get_stub();

	try {
		// Run setup.
		$v8->executeString( $stub, 'ssrBootstrap' );

		if ( $data ) {
			// Inject data.
			$v8->executeString( $data, 'ssrDataInjection' );
		}

		ob_start();
		$v8->executeString( $script );
		$output = ob_get_clean();

		return $output;
	} catch ( V8JsScriptException $error ) {
		if ( ! is_callable( $error_handler ) ) {
			$error_handler = __NAMESPACE__ . '\\handle_exception';
		}

		$hook = is_admin() ? 'admin_footer' : 'wp_footer';

		add_action( $hook, function() use ( $error_handler, $error ) {
			call_user_func( $error_handler, $error );
		} );
	}
}

/**
 * Render React.js component in server
 *
 * @param  string   $file          Path to build file.
 * @param  string   $component     Component to render.
 * @param  array    $props         Component props.
 * @param  string   $data          Optional data to add to the server env.
 * @param  callable $error_handler Optional function to call when error occures.
 * @param  string   $before        Anything to execute before rendering the component.
 * @param  string   $after         Anything to execute after rendering the component.
 *
 * @return mixed
 */
function react_ssr(
	$file,
	$component,
	array $props,
	$data = null,
	$error_handler = null,
	$before = null,
	$after = null
) {
	$script = file_get_contents( $file ); // phpcs:ignore

	if ( ! empty( $before ) ) {
		$script .= $before;
	}

	$script .= sprintf(
		"\nprint( global.ReactDOMServer.renderToString( React.createElement( %s, %s ) ) )",
		$component,
		wp_json_encode( $props )
	);

	if ( ! empty( $after ) ) {
		$script .= $after;
	}

	$output = exec_js( $script, $data, $error_handler );

	return $output;
}
