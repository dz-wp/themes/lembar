# Asset Loader

This is a helper for enqueueing assets with support for [Webpack DevServer](https://webpack.js.org/configuration/dev-server/). When Webpack DevServer is running and the manifest file is found, it will serve the assets from WDS as listed in the manifest.

For this to work, plugins/themes must have this structure for their assets:

```
myplugin/
  src/
    myplugin.js
  dist/
  plugin.php
```

Asset entries should then be added to webpack config:
```js
{
	// ...
	createEntries( {
		// myplugin: entry key
		// plugins/myplugin: plugin's src/ location relative to content directory.
		myplugin: 'plugins/myplugin',
	}, true ),
	// ...
}
```

They can then enqueue the assets from `plugin.php` like so:

```php
HM\Asset_Loader\enqueue([
	'entry'       => 'myplugin', // The entry key as added to webpack.
	'handle'      => 'myplugin', // Can be anything.
	'has_css'     => true,
	'dir_url'     => plugins_url( '', __FILE__ ),
	'dir_path'    => __DIR__,
	'script_deps' => [ 'some', 'script', 'dependencies' ],
	'style_deps'  => [ 'some', 'style', 'dependencies' ],
	'in_footer'   => false,
]);
```
