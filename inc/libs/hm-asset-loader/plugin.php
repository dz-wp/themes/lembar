<?php // phpcs:disable SlevomatCodingStandard

// phpcs:disable HM.Files.FunctionFileName.WrongFile
// phpcs:disable HM.Files.NamespaceDirectoryName.NoIncDirectory

/**
 * Plugin Name: HM Asset Loader
 * Plugin URI: https://humanmade.com
 * Description: Helper for loading scripts & styles with support for Webpack DevServer.
 * Author: Human Made
 * Author URI: https://humanmade.com
 * Version: 1.0.0
 */

namespace HM\Asset_Loader;

/**
 * Attempt to load a file at the specified path and parse its contents as JSON.
 *
 * @param string $path The path to the JSON file to load.
 * @return array|null;
 */
function load_asset_file( $path ) {
	if ( ! file_exists( $path ) ) {
		return null;
	}

	// phpcs:ignore
	$contents = file_get_contents( $path );

	if ( empty( $contents ) ) {
		return null;
	}

	return json_decode( $contents, true );
}

/**
 * Get assets list from manifest file
 *
 * Checks a directory for a root or build asset manifest file, and attempt to
 * decode and return the asset list JSON if found.
 *
 * @param string $directory Root directory containing `src` and `build` directory.
 *
 * @return array|null;
 */
function get_assets_list( string $manifest_path ) {
	$dev_assets = load_asset_file( $manifest_path );

	if ( empty( $dev_assets ) ) {
		return null;
	}

	return $dev_assets;
}

/**
 * Build assets sources
 *
 * @param array $args Arguments passed to enqueue().
 *
 * @return array
 */
function build_src( $args ) {
	$script_url = sprintf( '%s/dist/%s.js', $args['dir_url'], $args['entry'] );
	$srcs       = [
		'script' => $script_url,
	];

	$manifest_path = sprintf( '%s/dist/%s-manifest.json', $args['dir_path'], $args['entry'] );

	// If manifest file doesn't exist, we're in production mode.
	if ( ! file_exists( $manifest_path ) ) {
		$script_path     = sprintf( '%s/dist/%s.js', $args['dir_path'], $args['entry'] );
		$srcs['version'] = file_exists( $script_path ) ? filemtime( $script_path ) : null;

		if ( $args['has_css'] ) {
			$srcs['style'] = str_replace( '.js', '.css', $script_url );
		}

		if ( $args['css_only'] ) {
			unset( $srcs['script'] );
		}

		return $srcs;
	}

	$list = get_assets_list( $manifest_path );

	if ( empty( $list ) ) {
		return $srcs;
	}

	// This replaces the asset source with the one found in manifest file.
	array_walk( $srcs, function ( &$src ) use ( $list ) {
		$key = str_replace( content_url( '/' ), '', $src );

		if ( isset( $list[ $key ] ) ) {
			$src = $list[ $key ];
		}
	} );

	$srcs['version'] = filemtime( $manifest_path );

	/**
	 * Filter assets sources when running webpack dev server
	 *
	 * @param array $sources       Asset sources.
	 * @param array $manifest_list Manifest list.
	 */
	$srcs = apply_filters( 'hm_asset_loader_dev_src', $srcs, $list );

	return $srcs;
}

/**
 * Enqueue assets
 *
 * @param array $args {
 *     @type string  $entry       Entry name as added to webpack.
 *     @type string  $handle      Handle to register the asset.
 *     @type bool    $has_css     Whether or not this entry has a CSS output.
 *     @type bool    $css_only    Whether or not load CSS only on production.
 *     @type string  $dir_path    Absolute path of where the assets are located.
 *     @type string  $dir_url     Absolute URL of where the assets are located.
 *     @type array   $script_deps Script dependencies.
 *     @type array   $style_deps  Style dependencies.
 *     @type bool    $in_footer   Whether to enqueu the script in the footer instead of the head.
 * }
 *
 * @return bool
 */
function enqueue( array $args ) {
	$defaults = [
		'entry'         => '',
		'handle'        => '',
		'has_css'       => false,
		'css_only'      => false,
		'dir_path'      => '',
		'dir_url'       => '',
		'lang_dir_path' => '',
		'script_deps'   => [],
		'style_deps'    => [],
		'in_footer'     => true,
		'text_domain'   => '',
		'script_data'   => [],
	];

	if ( empty( $args['entry'] ) || empty( $args['dir_path'] ) || empty( $args['dir_url'] ) ) {
		return false;
	}

	if ( empty( $args['handle'] ) ) {
		$args['handle'] = $args['entry'];
	}

	$args = wp_parse_args( $args, $defaults );
	$srcs = build_src( $args );

	if ( isset( $srcs['script'] ) ) {
		wp_enqueue_script(
			$args['handle'],
			$srcs['script'],
			$args['script_deps'],
			$srcs['version'],
			$args['in_footer']
		);

		foreach ( $args['script_data'] as $data_var => $data_value ) {
			$data = is_callable( $data_value )
				? call_user_func( $data_value )
				: $data_value;

			wp_localize_script( $args['handle'], $data_var, $data );
		}
	}

	if ( ! empty( $args['text_domain'] ) && ! empty( $args['lang_dir_path'] ) ) {
		wp_set_script_translations(
			$args['handle'],
			$args['text_domain'],
			$args['lang_dir_path']
		);
	}

	if ( ! $args['has_css'] ) {
		return true;
	}

	if ( isset( $srcs['style'] ) ) {
		wp_enqueue_style(
			$args['handle'],
			$srcs['style'],
			$args['style_deps'],
			$srcs['version']
		);
	} else {
		/*
		 * Always enqueue style dependencies because in development mode,
		 * the style is not enqueued as it will be added to the head by webpack.
		 */
		foreach ( $args['style_deps'] as $style_dep ) {
			wp_enqueue_style( $style_dep );
		}
	}

	return true;
}
