<?php declare( strict_types = 1 );

namespace Lembar;

use WP;

/**
 * Bootstrap theme
 */
function bootstrap(): void {
	require_once __DIR__ . '/app.php';
	require_once __DIR__ . '/assets.php';
	require_once __DIR__ . '/data.php';

	App\bootstrap();
	Assets\bootstrap();
	Data\bootstrap();

	add_action( 'after_setup_theme', __NAMESPACE__ . '\\load_libraries' );
	add_action( 'after_setup_theme', __NAMESPACE__ . '\\register_features' );
	add_action( 'after_setup_theme', __NAMESPACE__ . '\\register_menu_locations' );
	add_action( 'parse_request', __NAMESPACE__ . '\\redirect_ugly_search' );
}

/**
 * Load libraries
 */
function load_libraries(): void {
	if ( ! function_exists( 'HM\\Asset_Loader\\enqueue' ) ) {
		require_once __DIR__ . '/libs/hm-asset-loader/plugin.php';
	}

	// phpcs:ignore SlevomatCodingStandard.ControlStructures.EarlyExit.EarlyExitNotUsed
	if ( ! function_exists( 'HM\\JS\\exec_js' ) ) {
		require_once __DIR__ . '/libs/hm-php-js.php';
	}
}

/**
 * Register theme features
 */
function register_features(): void {
	/* Automatic feed links. */
	add_theme_support( 'automatic-feed-links' );

	/* Let WordPress manage the document title. */
	add_theme_support( 'title-tag' );

	/* Post formats */
	add_theme_support( 'post-formats' );

	/* Enable support for Post Thumbnails on posts and pages. */
	add_theme_support( 'post-thumbnails' );

	/* Block editor - wide and full alignments. */
	add_theme_support( 'align-wide' );

	/* Block editor - maintain embed aspect ratios. */
	add_theme_support( 'responsive-embeds' );

	/* Block editor - disable setting font sizes by pixel size. */
	add_theme_support( 'disable-custom-font-sizes' );

	/* HTML5 gallery & caption. */
	add_theme_support(
		'html5',
		[
			'caption',
			'comment-form',
			'comment-list',
			'gallery',
			'search-form',
		],
	);
}

/**
 * Register menu locations
 */
function register_menu_locations(): void {
	register_nav_menus(
		[
			'main' => esc_html__( 'Main Menu', 'lembar' ),
		],
	);
}

/**
 * Redirect ugly search URL
 *
 * @param WP $wp WP object.
 */
function redirect_ugly_search( WP $wp ): void {
	if ( is_admin() ) {
		return;
	}

	// Already pretty, nothing to do.
	if ( strpos( $_SERVER['REQUEST_URI'], '/search/' ) === 0 ) {
		return;
	}

	$search = $wp->query_vars['s'] ?? null;

	if ( ! $search ) {
		return;
	}

	$path = "/search/{$search}";
	$page_num = absint( $wp->query_vars['paged'] ?? 0 );

	if ( $page_num ) {
		$path .= "/page/{$page_num}";
	}

	wp_safe_redirect( home_url( $path ) );
	exit;
}
