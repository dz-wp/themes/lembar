#!/bin/sh

set -e

mkdir -p $RELEASE_DIR/things

yarn install --production --pure-lockfile
yarn build

cp -r inc *.php style.css README.md composer.json package.json "${RELEASE_DIR}/"
cp -r things/dist "${RELEASE_DIR}/things/"

sed -i -e "s/9999999/$CI_COMMIT_TAG/" "${RELEASE_DIR}/style.css"
