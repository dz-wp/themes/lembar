const path = require( 'path' );
const crypto = require( 'crypto' );
const { unlinkSync } = require( 'fs' );
const { HotModuleReplacementPlugin } = require( 'webpack' );
const ManifestPlugin = require( 'webpack-manifest-plugin' );

const PUBLIC_PATH_PREFIX = '/__webpack_lembar';

// NOTE: This will be proxied to webpack's docker service by NGINX.
const getDevPublicPath = distPath => `${PUBLIC_PATH_PREFIX}/${distPath}`;

/**
 * Get the specified port on which to run the dev server
 */
function devServerPort() {
	return parseInt( process.env.PORT, 10 ) || 8888;
}

/**
 * Delete file
 *
 * @param {string} fileName Full path of filename to delete.
 */
function deleteFile( fileName ) {
	try {
		unlinkSync( fileName );
	} catch ( e ) {
		// Silently ignore unlinking errors: so long as the file is gone, that is good.
	}
}

const fileLoaderConfig = ( args = {} ) => ( {
	test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|eot|ttf)$/,
	loader: 'file-loader',
	options: {
		name: '[name].[sha1:hash:base36:8].[ext]',
		...args,
	},
} );

function createManifestInstances( entries, basePath ) {
	return entries.map(
		entryPoint =>
			new ManifestPlugin( {
				basePath,
				fileName: `${entryPoint}-manifest.json`,
				writeToFileEmit: true,
				publicPath: getDevPublicPath( basePath ),
			} ),
	);
}

/**
 * Create entries
 *
 * @param {String|Array}  entries       Entries object.
 * @param {String}        basePath      Base path or the entrypoints, relative to package.json.
 * @param {boolean}       isDevelopment Is this config for development?
 *
 * @return {Object} An object containing `entry`, `output` and `plugins` (when manifest is enabled).
 */
function createEntries( names, basePath, isDevelopment = false, extra = {} ) {
	const { isSSR = false, withWds = false } = extra;

	const distPath = `${basePath}/dist/`;
	const context = path.resolve( process.cwd(), basePath );
	const entryPoints = Array.isArray( names ) ? names : [ names ];
	const hash = crypto
		.createHash( 'md5' )
		.update( entryPoints.join( '' ) + basePath )
		.digest( 'hex' );

	const entry = entryPoints.reduce( ( carry, entryPoint ) => {
		const sources = [ `./src/${entryPoint}` ];

		if ( isDevelopment && withWds ) {
			sources.push(
				`webpack-hot-middleware/client?name=${hash}&path=${PUBLIC_PATH_PREFIX}/__webpack_hmr`,
			);
		}

		carry[entryPoint] = sources;

		return carry;
	}, {} );

	let result = {
		context,
		entry,
		name: hash,
		output: {
			path: `${context}/dist/`,
			publicPath: withWds ? getDevPublicPath( distPath ) : `/${distPath}`,
		},
		module: {
			rules: [
				fileLoaderConfig( {
					emitFile: ! isSSR,
					// When dev server is running, the path is absolute, otherwise, it's relative.
					publicPath: withWds ? getDevPublicPath( distPath ) : './',
				} ),
			],
		},
	};

	if ( isDevelopment && withWds && ! isSSR ) {
		result = {
			...result,
			plugins: [
				new HotModuleReplacementPlugin(),
				...createManifestInstances( entryPoints, distPath ),
			],
		};
	}

	return result;
}

module.exports = {
	devServerPort,
	deleteFile,
	fileLoaderConfig,
	createEntries,
};
