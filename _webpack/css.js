const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
const postCSSVars = require( 'postcss-simple-vars' );

const extraPostCSSPlugins = ( loader, plugins ) => plugins;

/**
 * Get PostCSS config
 *
 * https://github.com/postcss/postcss-simple-vars/issues/75#issuecomment-444741855
 *
 * @param {string}   isDevelopment  Is this for development?
 * @param {function} postCSSPlugins Extra PostCSS plugins.
 *
 * @return {Object}
 */
function getCSSConfig( args ) {
	const {
		dev: isDevelopment = false,
		wds: withWds = false,
		postCSSPlugins = extraPostCSSPlugins,
	} = args;

	const config = {
		module: {
			rules: [
				{
					test: /\.p?css$/,
					// NOTE: Order is important.
					use: [
						withWds && { loader: 'style-loader' },
						! withWds && MiniCssExtractPlugin.loader,
						{
							loader: 'css-loader',
							options: {
								importLoaders: 1,
								sourceMap: true,
							},
						},
						{
							loader: 'clean-css-loader',
							options: isDevelopment
								? { level: 0 }
								: {
									level: 2,
									inline: [ 'remote' ],
									format: {
										wrapAt: 200,
									},
								  },
						},
						{
							loader: 'postcss-loader',
							options: {
								ident: 'postcss',
								plugins: loader =>
									postCSSPlugins( loader, [
										require( 'postcss-custom-properties' ),
										require( 'postcss-nested-ancestors' ),
										require( 'postcss-nested' ),
										require( 'postcss-flexbugs-fixes' ),
										require( 'autoprefixer' ),
									] ),
							},
						},
					].filter( Boolean ),
				},
			],
		},
		plugins: [
			! withWds &&
				new MiniCssExtractPlugin( {
					filename: '[name].css',
				} ),
		].filter( Boolean ),
	};

	return config;
}

/**
 * Get PostCSS variables as object
 *
 * @param {Object} loader Loader object.
 * @param {string} path   Path to variables file.
 *
 * @return {Object}
 */
function getPostCSSVars( loader, path ) {
	const vars = require.resolve( path );
	loader.addDependency( vars );
	delete require.cache[vars];

	return postCSSVars( {
		variables: () => require( vars ),
	} );
}

module.exports = {
	getCSSConfig,
	getPostCSSVars,
};
