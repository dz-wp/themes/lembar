# Webpack Configuration

[Multi-compiler](https://github.com/webpack/webpack/tree/master/examples/multi-compiler) webpack configuration.


## config.js
This is the main configuration file where each plugin/theme's config should be added to.

## css.js
Configuration for CSS files. It supports PostCSS and other goodness.

## shared.js
Configuration for dealing with JS files and others.
