const path = require( 'path' );
const onExit = require( 'signal-exit' );
const express = require( 'express' );
const webpack = require( 'webpack' );
const ManifestPlugin = require( 'webpack-manifest-plugin' );
const webpackDevMiddleware = require( 'webpack-dev-middleware' );
const webpackHotMiddleware = require( 'webpack-hot-middleware' );

const { deleteFile, devServerPort } = require( './utils' );
const config = require( './config' )( {
	dev: true,
	rhl: true,
	wds: true,
} );

const cwd = process.cwd();

config.forEach( ( { plugins = [] } ) => {
	plugins.forEach( item => {
		if ( item instanceof ManifestPlugin ) {
			const { basePath, fileName } = item.opts;

			onExit( () => deleteFile( path.resolve( cwd, basePath + fileName ) ) );
		}
	} );
} );

const app = express();
const port = devServerPort();
const compiler = webpack( config );

app.use(
	webpackDevMiddleware( compiler, {
		port,
		hot: true,
		publicPath: '/',
		headers: {
			'Access-Control-Allow-Origin': '*',
		},
		watchOptions: {
			aggregateTimeout: 500,
			poll: 1000,
		},
		// See https://github.com/webpack/webpack-dev-server/issues/1604
		host: '0.0.0.0',
		disableHostCheck: true,
		writeToDisk: filePath => ! /hot-update/.test( filePath ),
	} ),
);
app.use(
	webpackHotMiddleware( compiler, {
		path: '/__webpack_lembar/__webpack_hmr',
	} ),
);

// Serve the files on port 3000.
// eslint-disable-next-line no-console
app.listen( port, () => console.log( `Dev server running on port ${port}` ) );
