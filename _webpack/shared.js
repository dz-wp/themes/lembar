const { CleanWebpackPlugin } = require( 'clean-webpack-plugin' );

/**
 * Get shared config
 *
 * @param {Object} args Args passed to webpack.
 *
 * @return {Object}
 */
function getSharedConfig( args ) {
	const {
		dev: isDevelopment = false,
		rhl = false,
	} = args;

	const env = isDevelopment && rhl ? 'development' : 'production';
	const alias = rhl ? { 'react-dom': '@hot-loader/react-dom' } : {};

	return {
		mode: env,
		devtool: isDevelopment ? 'cheap-module-eval-source-map' : 'hidden-source-map',
		output: {
			filename: '[name].js',
			libraryTarget: 'this',
			publicPath: '/',
		},
		resolve: {
			extensions: [ '.js' ],
			alias,
		},
		module: {
			strictExportPresence: true,
			rules: [
				{
					test: /\.jsx?$/,
					loader: 'babel-loader',
					exclude: /(node_modules|bower_components)/,
					options: {
						babelrc: false,
						cacheDirectory: true,
						presets: [ [ '@babel/preset-env', { modules: false } ] ],
						plugins: [
							[ 'lodash' ],
							'@babel/plugin-proposal-class-properties',
							'@babel/plugin-proposal-object-rest-spread',
							'@babel/plugin-transform-react-jsx',
							[
								'@babel/plugin-transform-runtime',
								{
									corejs: 2,
									helpers: true,
									useESModules: false,
								},
							],
							isDevelopment && rhl && 'react-hot-loader/babel',
						].filter( Boolean ),
					},
				},
			],
		},
		performance: {
			assetFilter: assetFilename => ! /\.map$/.test( assetFilename ),
			...( isDevelopment
				? {
					maxAssetSize: 3145728, // 3 mB.
					maxEntrypointSize: 3145728,
				  }
				: {}
			),
		},
		plugins: [
			new CleanWebpackPlugin(),
		],
	};
}

module.exports = getSharedConfig;
