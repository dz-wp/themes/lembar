import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.pcss';

export default function ScreenReaderText( props ) {
	const { TagName, children, className, ...rest } = props;
	const finalClassName = classnames( className, 'screen-reader-text' );

	return (
		<TagName className={ finalClassName } { ...rest }>
			{ children }
		</TagName>
	);
}

ScreenReaderText.defaultProps = {
	className: '',
	TagName: 'span',
};

ScreenReaderText.propTypes = {
	children: PropTypes.any.isRequired,
	className: PropTypes.string,
	TagName: PropTypes.string,
};
