import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { __ } from '@wordpress/i18n';
import { SearchForm } from 'malimbu/components';

import ScreenReaderText from '../ScreenReaderText';

import './style.pcss';

export function SiteFooter( props ) {
	const { text, url } = props;
	const date = new Date();
	const year = date.getFullYear();
	const searchLabel = <ScreenReaderText>{ __( 'Search for:', 'lembar' ) }</ScreenReaderText>;

	return (
		<footer className="site-footer" role="contentinfo">
			<SearchForm id="search-form" label={ searchLabel } />
			<p>&copy; { year } <Link to={ url }>{ text }</Link></p>
		</footer>
	);
}

SiteFooter.defaultProps = {
	url: '/',
};

SiteFooter.propTypes = {
	url: PropTypes.string,
	text: PropTypes.string.isRequired,
};

export default connect( state => ( {
	text: state.info.name,
} ) )( SiteFooter );
