import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import init from 'malimbu/utils/init';
import { EnvProvider } from 'malimbu/components';
import { AssetLoader, Markup, Prism } from 'malimbu/browser-components';

import App from './App';
import extraReducers from './store/reducers';

const { appRoutes, store } = init( global.__lembar_data__, { extraReducers } );
const contextValue = {
	isServer: false,
	components: {
		AssetLoader,
		Markup,
		Prism,
	},
};
const root = document.getElementById( 'root' );
const render = 'rendered' in root.dataset
	? ReactDOM.hydrate
	: ReactDOM.render;

render(
	<EnvProvider value={ contextValue }>
		<Provider store={ store }>
			<BrowserRouter>
				<App routes={ appRoutes } />
			</BrowserRouter>
		</Provider>
	</EnvProvider>,
	root,
);
