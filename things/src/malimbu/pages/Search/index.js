import { hot } from 'react-hot-loader';

import { generateDocTitle } from '../../utils/helpers';
import { EntryList, withArchiveRoute, withDocTitle } from '../../components';

function mapPropsToDocTitle( props ) {
	const { page, match, siteName } = props;
	const { params } = match;
	const { search } = params;

	const title = generateDocTitle( siteName, '', page, `Search Results for “${ search }”` );

	return title;
}

function mapPropsToParams( props ) {
	const { match } = props;
	const { params } = match;
	const { search } = params;

	return { search };
}

const Search = withArchiveRoute(
	'search',
	mapPropsToParams
)( withDocTitle( mapPropsToDocTitle )( EntryList ) );

export default hot( module )( Search );
