import React from 'react';
import PropTypes from 'prop-types';
import { hot } from 'react-hot-loader';

import { routeShape } from '../../shapes';
import { getHandler } from '../../store/types';
import { Entry, with404, withDocTitle, withSingleBySlug } from '../../components';

function mapPropsToDocTitle( props ) {
	const { post, siteName } = props;

	if ( ! post ) {
		return null;
	}

	const { title } = post;
	const { rendered } = title;

	return `${ rendered } – ${ siteName }`;
}

export function Single( props ) {
	const { match, type, paramsKey } = props;
	const { params } = match;
	const slug = params[ paramsKey ];

	const Component = withSingleBySlug(
		getHandler( type ),
		state => state[ type ],
		slug,
	)( withDocTitle( mapPropsToDocTitle )( Entry ) );

	return <Component isSingle />;
}

Single.propTypes = {
	...routeShape,
	paramsKey: PropTypes.string,
	type: PropTypes.string,
};

export default hot( module )( with404()( Single ) );
