export { default as Home } from './Home';
export { default as Search } from './Search';
export { default as Single } from './Single';
export { default as Term } from './Term';
