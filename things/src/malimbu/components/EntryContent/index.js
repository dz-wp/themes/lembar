import React from 'react';
import PropTypes from 'prop-types';

import { postShape } from '../../shapes';
import { Content } from '../';

export default function EntryContent( props ) {
	const { post } = props;
	const { content, description, type } = post;
	const itemContent = type === 'attachment' ? description : content;

	return (
		<div className="entry-content">
			<Content content={ itemContent.rendered } />
		</div>
	);
}

EntryContent.propTypes = {
	isSingle: PropTypes.bool,
	post: PropTypes.shape( postShape ),
};
