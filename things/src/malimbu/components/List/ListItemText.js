import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import decode from 'simple-entity-decode';

export default function ListItemText( props ) {
	const { title } = props;
	const text = decode( title );

	return <Fragment>{ text }</Fragment>;
}

ListItemText.propTypes = {
	title: PropTypes.string.isRequired,
};
