import React from 'react';
import PropTypes from 'prop-types';

export default function ListItemLink( props ) {
	const { className, url, children } = props;

	return (
		<a href={ url } className={ className }>
			{ children }
		</a>
	);
}

ListItemLink.defaultProps = {
	className: 'link',
};

ListItemLink.propTypes = {
	children: PropTypes.any.isRequired,
	className: PropTypes.string,
	url: PropTypes.string.isRequired,
};
