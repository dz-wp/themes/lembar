import React from 'react';
import PropTypes from 'prop-types';

import { itemComponents, wrapShape } from './shapes';

export default function Wrap( props ) {
	const { className, Item, items, ...rest } = props;

	return (
		<ul className={ className }>
			{ items.map( ( item, index ) => (
				<Item
					key={ `${ index }-${ item.title }` }
					Item={ Item }
					{ ...item }
					{ ...rest }
				/>
			) ) }
		</ul>
	);
}

Wrap.defaultProps = {
	className: '',
};

Wrap.propTypes = {
	...wrapShape,
	...itemComponents,
	Item: PropTypes.elementType.isRequired,
};
