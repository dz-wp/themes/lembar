import PropTypes from 'prop-types';

export const itemComponents = {
	Link: PropTypes.elementType.isRequired,
	Text: PropTypes.elementType.isRequired,
	Wrap: PropTypes.elementType.isRequired,
};

const singleItemShape = {
	url: PropTypes.string,
	title: PropTypes.string.isRequired,
};

export const itemShape = {
	...singleItemShape,
	subItems: PropTypes.arrayOf( PropTypes.shape( singleItemShape ) ),
};

export const wrapShape = {
	className: PropTypes.string,
	items: PropTypes.arrayOf( PropTypes.shape( itemShape ) ).isRequired,
};
