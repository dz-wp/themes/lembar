import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { postShape, searchResultShape, typeShape } from '../../shapes';
import { resolve } from '../../utils/helpers';
import ArchivePagination from '../ArchivePagination';
import Entry from '../Entry';
import EntryById from '../EntryById';
import NotFound from '../NotFound';
import withPageLoader from '../with-page-loader';

export function EntryList( props ) {
	const {
		className,
		archiveId,
		hasMore,
		page,
		posts,
		types,
	} = props;
	const finalClassName = resolve( className, props );

	if ( ! posts.length ) {
		return <NotFound />;
	}

	return (
		<Fragment>
			<div className={ finalClassName }>
				{ posts.map( post => {
					const { id, subtype } = post;

					if ( subtype ) {
						return (
							<EntryById
								key={ id }
								id={ id }
								isSingle={ false }
								type={ types[ subtype ].rest_base }
							/>
						);
					}

					return <Entry key={ id } isSingle={ false } post={ post } />;
				} ) }
			</div>
			{ ( hasMore || page > 1 ) && (
				<ArchivePagination archiveId={ archiveId } hasMore={ hasMore } page={ page } />
			) }
		</Fragment>
	);
}

EntryList.defaultProps = {
	className: 'entry-list',
	hasMore: false,
	page: 1,
	posts: [],
};

EntryList.propTypes = {
	archiveId: PropTypes.string.isRequired,
	className: PropTypes.oneOfType( [
		PropTypes.func,
		PropTypes.string,
	] ),
	hasMore: PropTypes.bool,
	page: PropTypes.number,
	posts: PropTypes.oneOfType( [
		PropTypes.arrayOf( PropTypes.shape( postShape ) ),
		PropTypes.arrayOf( PropTypes.shape( searchResultShape ) ),
	] ),
	types: PropTypes.objectOf( PropTypes.shape( typeShape ) ).isRequired,
};

const mapPropsToLoading = ( { loading, posts } ) => ( loading || ! posts );
const mapStateToProps = state => ( { types: state.types } );

export default withPageLoader( mapPropsToLoading )(
	connect( mapStateToProps )( EntryList )
);
