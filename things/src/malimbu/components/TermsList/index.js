import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { postShape } from '../../shapes';
import { createSingleHOC } from '../../utils/helpers';
import TermLink from '../TermLink';

export function TermsList( props ) {
	const { className, taxonomy, terms, title } = props;

	if ( ! terms || ! terms.length ) {
		return null;
	}

	return (
		<ul aria-label={ title } className={ `entry-tags entry-tags--${ taxonomy } ${ className }` }>
			{ terms.map( id => {
				const Tag = createSingleHOC( taxonomy, id )( TermLink );

				return (
					<li key={ `tag-${ id }` } className="entry-tags-item">
						<Tag className="entry-tags-item-link" />
					</li>
				);
			} ) }
		</ul>
	);
}

TermsList.defaultProps = {
	className: 'terms-list',
	terms: [],
	title: '',
};

TermsList.propTypes = {
	className: PropTypes.string,
	post: PropTypes.shape( postShape ).isRequired,
	taxonomy: PropTypes.string.isRequired,
	terms: PropTypes.arrayOf( PropTypes.number ),
	title: PropTypes.string,
};

function mapStateToProps( state, ownProps ) {
	const { post, taxonomy, title } = ownProps;
	const { [ taxonomy ]: terms } = post;

	if ( ! terms || ! terms.length ) {
		return {};
	}

	const { taxonomies } = state;
	const taxObject = Object.values( taxonomies ).find( t => t.rest_base === taxonomy );

	if ( ! taxObject ) {
		return {};
	}

	return {
		terms,
		title: title || taxObject.name,
	};
}

export default connect( mapStateToProps )( TermsList );
