import React from 'react';
import PropTypes from 'prop-types';

import Entry from '../Entry';
import { createSingleHOC } from '../../utils/helpers';

export default function EntryById( props ) {
	const { id, type } = props;
	const Component = createSingleHOC( type, id )( Entry );

	return <Component { ...props } />;
}

EntryById.propTypes = {
	id: PropTypes.number.isRequired,
	type: PropTypes.string.isRequired,
};
