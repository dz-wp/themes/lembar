import React from 'react';
import PropTypes from 'prop-types';
import { __ } from '@wordpress/i18n';
import { Link } from 'react-router-dom';

export default function ArchivePagination( props ) {
	const { archiveId, className, hasMore, page } = props;
	const path = archiveId === 'home' ? '' : `/${ archiveId }`;
	const newerPath = page > 2 ? `${ path }/page/${ page - 1 }` : path;
	const olderPath = `${ path }/page/${ page + 1 }`;

	return (
		<div className={ className }>
			{ page > 1 && (
				<Link
					className="pagination__link pagination__link--newer"
					to={ newerPath }
				>
					{ __( '← Newer posts', 'lembar' ) }
				</Link>
			) }
			{ ( hasMore && page > 1 ) && (
				<span className="pagination__separator">&nbsp;</span>
			) }
			{ hasMore && (
				<Link
					to={ olderPath }
					className="pagination__link pagination__link--older"
				>
					{ __( 'Older posts →', 'lembar' ) }
				</Link>
			) }
		</div>
	);
}

ArchivePagination.defaultProps = {
	className: 'pagination',
};

ArchivePagination.propTypes = {
	archiveId: PropTypes.string.isRequired,
	className: PropTypes.string,
	hasMore: PropTypes.bool.isRequired,
	page: PropTypes.number.isRequired,
};
