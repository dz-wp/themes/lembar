import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { routeShape } from '../../shapes';
import { set404 } from '../../store/actions/routes';
import NotFound from '../NotFound';
import withDocTitle from '../with-doc-title';

function Error404( { siteName } ) {
	const Content = withDocTitle( `Page not found – ${siteName}` )( NotFound );

	return <Content />;
}

Error404.propTypes = {
	siteName: PropTypes.string.isRequired,
};

export default function with404() {
	return WrappedComponent => {
		class With404 extends Component {
			componentDidUpdate( prevProps ) {
				const { dispatch, location } = this.props;
				const { location: prevLocation } = prevProps;

				if ( location.pathname !== prevLocation.pathname ) {
					dispatch( set404( false ) );
				}
			}

			componentWillUnmount() {
				const { is404, dispatch } = this.props;

				if ( is404 ) {
					dispatch( set404( false ) );
				}
			}

			render() {
				const { is404, siteName, ...rest } = this.props;

				if ( is404 ) {
					return <Error404 { ...this.props } />;
				}

				return <WrappedComponent { ...rest } />;
			}
		}

		With404.propTypes = {
			...routeShape,
			dispatch: PropTypes.func.isRequired,
			is404: PropTypes.bool.isRequired,
		};

		return connect( ( { info, routes } ) => ( {
			siteName: info.name,
			is404: routes.is404,
		} ) )( With404 );
	};
}
