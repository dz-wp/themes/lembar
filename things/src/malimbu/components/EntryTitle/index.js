import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import decode from 'simple-entity-decode';

import { postShape } from '../../shapes';

export default function EntryTitle( props ) {
	const CLASSNAME = 'entry-title';
	const { post, isSingle } = props;
	const { routePath, title } = post;
	const { rendered: renderedTitle } = title;
	const entryTitle = decode( renderedTitle );

	if ( isSingle ) {
		return <h1 className={ CLASSNAME }>{ entryTitle }</h1>;
	}

	return (
		<h2 className={ CLASSNAME }>
			<Link className={ `${ CLASSNAME }-link` } to={ routePath }>{ entryTitle }</Link>
		</h2>
	);
}

EntryTitle.defaultProps = {
	isSingle: false,
};

EntryTitle.propTypes = {
	post: PropTypes.shape( postShape ),
	isSingle: PropTypes.bool,
};
