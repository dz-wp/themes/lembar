import React, { createContext } from 'react';

const EnvContext = createContext( {
	isServer: true,
} );

const { Consumer, Provider } = EnvContext;

export { Provider as EnvProvider };

export default function withEnvContext( mapContextToProps ) {
	return Component => props => (
		<Consumer>
			{ context => (
				<Component
					{ ...props }
					{ ...mapContextToProps( context, props ) }
				/>
			) }
		</Consumer>
	);
}
