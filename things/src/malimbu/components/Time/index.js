import React from 'react';
import PropTypes from 'prop-types';
import { format, parse } from 'date-fns';

export default function Time( props ) {
	const { date, ...rest } = props;
	// eslint-disable-next-line quotes
	const dateObject = parse( date, "yyyy-MM-dd'T'HH:mm:ss", new Date() );
	const dateFormatted = format( dateObject, 'd MMM yyyy' );

	return (
		<time dateTime={ date } { ...rest }>
			{ dateFormatted }
		</time>
	);
}

Time.propTypes = {
	date: PropTypes.string.isRequired,
};
