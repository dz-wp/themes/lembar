import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { toggleMenu } from '../../store/actions/menus';
import { resolve } from '../../utils/helpers';
import List from '../List';
import Item from './Item';
import Link from './Link';

function createListProps( props ) {
	const { listComponents, menu, menuClassName } = props;
	const { isOpen, items } = menu;

	const menuClassNameSuffix = isOpen ? 'open' : 'closed';
	const finalMenuClassName = [
		resolve( menuClassName, props ),
		`menu--is-${ menuClassNameSuffix }`,
	].join( ' ' );

	return {
		items,
		className: finalMenuClassName,
		...{
			Item,
			Link,
			...listComponents,
		},
	};

}

export function NavMenu( props ) {
	const {
		Button,
		className,
		container: ContainerTag,
		id,
		menu = {},
		onToggle,
	} = props;
	const {
		isOpen,
		items = [],
		name,
	} = menu;

	if ( ! items.length ) {
		return null;
	}

	const navProps = {
		'aria-label': name,
		className: resolve( className, props ),
		role: 'navigation',
	};

	if ( id ) {
		navProps.id = id;
	}

	return (
		<ContainerTag { ...navProps }>
			{ Button && (
				<Button { ...props } onClick={ () => onToggle( ! isOpen ) } />
			) }
			<List { ...createListProps( props ) } />
		</ContainerTag>
	);
}

NavMenu.defaultProps = {
	className: 'nav-menu',
	container: 'nav',
	listComponents: {},
	menuClassName: 'menu',
};

NavMenu.propTypes = {
	Button: PropTypes.elementType,
	className: PropTypes.oneOfType( [
		PropTypes.string,
		PropTypes.func,
	] ),
	container: PropTypes.oneOf( [
		'div',
		'nav',
	] ),
	id: PropTypes.string,
	listComponents: PropTypes.shape( {
		Item: PropTypes.elementType,
		Link: PropTypes.elementType,
		Text: PropTypes.elementType,
		Wrap: PropTypes.elementType,
	} ),
	location: PropTypes.string.isRequired,
	menu: PropTypes.shape( {
		isOpen: PropTypes.bool.isRequired,
		items: PropTypes.array.isRequired,
		name: PropTypes.string.isRequired,
	} ),
	menuClassName: PropTypes.oneOfType( [
		PropTypes.string,
		PropTypes.func,
	] ),
	onToggle: PropTypes.func.isRequired,
};

function mapStateToProps( state, ownProps ) {
	const { location } = ownProps;
	const { menus } = state;
	const { [ location ]: menu } = menus;

	if ( ! menu ) {
		return {};
	}

	return { menu };
}

function mapDispatchToProps( dispatch, ownProps ) {
	const { location } = ownProps;

	return {
		onToggle: isOpen => dispatch( toggleMenu( location, isOpen ) ),
	};
}

export default connect( mapStateToProps, mapDispatchToProps )( NavMenu );
