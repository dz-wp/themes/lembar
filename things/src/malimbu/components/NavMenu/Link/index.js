import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

export default function Link( props ) {
	const { url, children } = props;

	return <NavLink className="menu-item-link" exact to={ url }>{ children }</NavLink>;
}

Link.propTypes = {
	children: PropTypes.any.isRequired,
	url: PropTypes.string.isRequired,
};
