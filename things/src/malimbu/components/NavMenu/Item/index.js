import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import { routeShape } from '../../../shapes';
import ListItem from '../../List/ListItem';

export function Item( props ) {
	const { classes, location, url } = props;
	const { pathname } = location;
	const className = [
		'menu-item',
		( pathname === url && 'current-menu-item' ),
		...classes,
	].filter( Boolean ).join( ' ' ).trim();

	return <ListItem { ...props } className={ className } />;
}

Item.defaultProps = {
	classes: [],
};

Item.propTypes = {
	classes: PropTypes.arrayOf( PropTypes.string ),
	url: PropTypes.string.isRequired,
	...routeShape,
};

export default withRouter( Item );
