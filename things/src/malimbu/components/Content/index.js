import React from 'react';
import PropTypes from 'prop-types';

import withEnvContext from 'malimbu/components/with-env-context';

export function Content( props ) {
	const { components, isServer, ...rest } = props;

	if ( isServer ) {
		return <div dangerouslySetInnerHTML={ { __html: rest.content } } />;
	}

	const { Markup } = components;

	return <Markup tagName="div" { ...rest } />;
}

Content.propTypes = {
	components: PropTypes.objectOf( PropTypes.elementType ),
	content: PropTypes.string.isRequired,
	isServer: PropTypes.bool.isRequired,
};

export default withEnvContext( context => ( { ...context } ) )( Content );
