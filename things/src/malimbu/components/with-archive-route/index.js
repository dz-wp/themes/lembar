import React from 'react';
import { connect } from 'react-redux';
import { withPagedArchive } from '@humanmade/repress';

import { createArchive, getHandler } from '../../store/types';
import { generateArchiveId, resolve } from '../../utils/helpers';

function mapPropsToId( type, params, props ) {
	const { archives } = getHandler( type );
	const { match, per_page } = props;
	const { url } = match;

	const archiveId = generateArchiveId( url );

	if ( ! ( archiveId in archives ) ) {
		createArchive( type, archiveId, {
			...resolve( params, props ),
			per_page,
		} );
	}

	return archiveId;
}

function getPage( props ) {
	const { match } = props;
	const { params } = match;
	const { page = 1 } = params;

	return Number( page );
}

function mapStateToProps( state ) {
	const { info } = state;
	const { settings } = info;
	const { posts_per_page: per_page } = settings;

	return { per_page };
}

export default function withArchiveRoute( type, params ) {
	return WrappedComponent => props => {
		const Component = connect( mapStateToProps )( withPagedArchive(
			getHandler( type ),
			state => state[ type ],
			( ...args ) => mapPropsToId( type, params, ...args ),
			{ getPage }
		)( WrappedComponent ) );

		return <Component { ...props } />;
	};
}
