import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import createMiddlewares from './middlewares';
import getRootReducer from './reducers';

/**
 * Configure store
 *
 * @param {Object}     initialState     Initial state to bootstrap our stores with for server-side rendering.
 * @param {function[]} extraMiddlewares Extra store middlewares.
 * @param {function}   extraReducers    Extra reducers.
 *
 * @return {Object}
 */
export default function configureStore( initialState, extraMiddlewares = [], extraReducers = () => {} ) {
	const middlewares = [
		thunk,
		...createMiddlewares( initialState ),
		...extraMiddlewares,
	];
	const finalCreateStore = applyMiddleware( ...middlewares )( createStore );
	const store = finalCreateStore( getRootReducer( initialState, extraReducers ), initialState );

	if ( module.hot ) {
		// Enable Webpack hot module replacement for reducers.
		module.hot.accept( './reducers', () => {
			const nextReducer = require( './reducers' );

			store.replaceReducer( nextReducer.default );
		} );
	}

	return store;
}
