export default function routes( initialState ) {
	const { taxonomies, types } = initialState;
	let actionTypes = [];

	Object.values( taxonomies ).forEach( ( { rest_base } ) => {
		const TYPE = rest_base.toUpperCase();

		actionTypes = actionTypes.concat( [
			`LOAD_${ TYPE }_ERROR`,
			`LOAD_${ TYPE }_BY_SLUG_ERROR`,
		] );
	} );

	Object.values( types ).forEach( ( { rest_base } ) => {
		const TYPE = rest_base.toUpperCase();

		actionTypes = actionTypes.concat( [
			`LOAD_${ TYPE }_ERROR`,
			`LOAD_${ TYPE }_BY_SLUG_ERROR`,
		] );
	} );

	return function routes( state = {}, action ) {
		const { is404, type } = action;

		if ( actionTypes.indexOf( type ) >= 0 ) {
			return {
				is404: true,
			};
		}

		switch ( action.type ) {
			case 'SET_404':
				return {
					is404,
				};

			default:
				return state;
		}
	};
}
