import { combineReducers } from 'redux';

import externalAssets from './external-assets';
import menus from './menus';
import routes from './routes';
import { getTypesReducers } from '../types';

const dummy = ( state = {} ) => state;

export default function getRootReducer( initialState, extraReducers = () => {} ) {
	return combineReducers( {
		externalAssets,
		menus,
		info: dummy,
		routes: routes( initialState ),
		taxonomies: dummy,
		types: dummy,
		...getTypesReducers( initialState ),
		...extraReducers( initialState ),
	} );
}
