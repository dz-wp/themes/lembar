import { handler, mergePosts } from '@humanmade/repress';

const DEFAULT_STATE = {
	loadingPostBySlug: [],
};

export default class TypesHandler extends handler {
	constructor( options ) {
		super( options );

		const upperType = options.type.toUpperCase();

		this.actions = {
			...this.actions,
			getBySlugStart: `LOAD_${ upperType }_BY_SLUG_REQUEST`,
			getBySlugSuccess: `LOAD_${ upperType }_BY_SLUG`,
			getBySlugError: `LOAD_${ upperType }_BY_SLUG_ERROR`,
		};
	}

	/**
	 * Action creator to fetch a single post.
	 *
	 * @param {String} slug    Post slug.
	 * @param {String} context Context to fetch.
	 *
	 * @return {Function} Action to dispatch.
	 */
	fetchSingleBySlug = ( slug, context = 'view' ) => dispatch => {
		dispatch( {
			type: this.actions.getBySlugStart,
			slug,
		} );

		return this.fetch( this.url, {
			context,
			slug,
		} )
			.then( data => {
				if ( ! data || ( Array.isArray( data ) && ! data.length ) ) {
					dispatch( {
						type: this.actions.getBySlugError,
						slug,
						error: 'Not found.',
					} );
				} else {
					dispatch( {
						type: this.actions.getBySlugSuccess,
						slug,
						data,
					} );
				}

				return slug;
			} )
			.catch( error => {
				dispatch( {
					type: this.actions.getBySlugError,
					slug,
					error,
				} );

				// Rethrow for other promise handlers.
				if ( this.rethrow ) {
					throw error;
				}
			} );
	}

	/**
	 * Get a single post from the store by slug.
	 *
	 * @param {object} substate Substate registered for the type.
	 * @param {String} slug     Post slug.
	 *
	 * @return {Object|null} Post if available, otherwise null.
	 */
	getSingleBySlug( substate, slug ) {
		if ( ! substate.posts ) {
			return null;
		}

		return substate.posts.find( post => post.slug === slug );
	}

	/**
	 * Is the post currently being loaded by slug?
	 *
	 * @param {object} substate Substate registered for the type.
	 * @param {Number} slug     Post ID.
	 *
	 * @return {Boolean} True if the post is currently being loaded, false otherwise.
	 */
	isPostLoadingBySlug( substate, slug ) {
		return substate.loadingPostBySlug.indexOf( slug ) >= 0;
	}

	/**
	 * Reducer for the substate.
	 *
	 * This needs to be added to your store to be functional.
	 *
	 * @param {object} state  Store state to reduce against.
	 * @param {object} action Action being dispatched.
	 *
	 * @return {object} Reduced state.
	 */
	reducerExtended = ( state, action ) => {
		const { data, slug, type } = action;

		switch ( type ) {
			case this.actions.getBySlugStart:
				return {
					...state,
					loadingPostBySlug: [
						...state.loadingPostBySlug,
						slug,
					],
				};

			case this.actions.getBySlugSuccess:
				return {
					...state,
					loadingPostBySlug: state.loadingPostBySlug.filter( p => p !== slug ),
					posts: mergePosts( state.posts, data ),
				};

			case this.actions.getBySlugError:
				return {
					...state,
					loadingPostBySlug: state.loadingPostBySlug.filter( p => p !== slug ),
				};

			default: {
				if ( ! state || ! state._initialized ) {
					return this.reducer( {
						...DEFAULT_STATE,
						...( state || {} ),
					}, action );
				}

				return this.reducer( state, action );
			}
		}
	}
}
