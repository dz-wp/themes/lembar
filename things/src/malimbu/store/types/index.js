import Handler from './handler';

const handlers = {};

function getEndpoint( type ) {
	return type._links['wp:items'][0].href;
}

export function createArchive( type, id, params = {} ) {
	const handler = getHandler( type );

	handler.registerArchive( id, params );

	return handler.fetchArchive( id );
}

export function createHandler( type, url ) {
	const handler = handlers[ type ] = new Handler( {
		type,
		url,
	} );

	return handler;
}

export function getHandler( type ) {
	return handlers[ type ];
}

export function getTypesReducers( initialState ) {
	const { info, taxonomies, types } = initialState;
	const { apiUrl } = info;
	const reducers = {};

	// Post Types.
	Object.values( types ).forEach( type => {
		const { rest_base } = type;
		const endpoint = getEndpoint( type );
		const handler = createHandler( rest_base, endpoint );

		reducers[ rest_base ] = handler.reducerExtended;
	} );

	// Taxonomies.
	Object.values( taxonomies ).forEach( tax => {
		const { rest_base } = tax;
		const endpoint = getEndpoint( tax );
		const handler = createHandler( rest_base, endpoint );

		reducers[ rest_base ] = handler.reducerExtended;
	} );

	[ 'search' ].forEach( type => {
		const handler = createHandler( type, `${apiUrl}/wp/v2/${type}` );
		reducers[ type ] = handler.reducerExtended;
	} );

	return reducers;
}
