import { TOGGLE_MENU } from '../reducers/menus';

export function toggleMenu( location, isOpen ) {
	return {
		type: TOGGLE_MENU,
		location,
		isOpen,
	};
}
