import { TOGGLE_EXTERNAL_ASSET } from '../reducers/external-assets';

export function toggleExternalAsset( id, loaded = false ) {
	return {
		id,
		loaded,
		type: TOGGLE_EXTERNAL_ASSET,
	};
}
