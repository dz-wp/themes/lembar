export function set404( is404 ) {
	return {
		type: 'SET_404',
		is404,
	};
}
