import React from 'react';
import Interweave from 'interweave';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import transform from './transform';

export function Markup( props ) {
	const { siteInfo, ...rest } = props;
	const finalProps = {
		transform: ( ...args ) => transform( siteInfo, ...args ),
		tagName: 'fragment',
		...rest,
	};

	return <Interweave { ...finalProps } />;
}

Markup.propTypes = {
	siteInfo: PropTypes.objectOf( PropTypes.string ).isRequired,
};

function mapStateToProps( state ) {
	const { info } = state;
	const { baseUploadUrl, url } = info;

	return {
		siteInfo: {
			url,
			baseUploadUrl,
		},
	};
}

export default connect( mapStateToProps )( Markup );
