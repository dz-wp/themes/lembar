import React from 'react';
import { Link } from 'react-router-dom';

import { collectNodeAttributes } from '../../../utils/helpers';

export default function internalLink( siteInfo, node, children ) {
	const { baseUploadUrl, url } = siteInfo;
	let href = node.getAttribute( 'href' );

	if ( href.indexOf( url ) !== 0 ) {
		return;
	}

	if ( href.indexOf( baseUploadUrl ) === 0 ) {
		return;
	}

	return (
		<Link
			to={ href.replace( url, '' ) }
			{ ...collectNodeAttributes( node, [ 'href' ] ) }
		>
			{ children }
		</Link>
	);
}
