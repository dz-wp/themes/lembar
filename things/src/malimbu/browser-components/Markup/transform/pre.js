import React from 'react';
import classnames from 'classnames';

import {
	collectNodeAttributes,
	getCodeLanguage,
	getPrismComponents,
} from '../../../utils/helpers';

export default function pre( siteInfo, node, children ) {
	const language = getCodeLanguage( node.className );

	if ( getPrismComponents().indexOf( language ) >= 0 ) {
		return;
	}

	const props = {
		...collectNodeAttributes( node ),
		className: classnames( [
			...Array.from( node.classList ),
			'language-none',
		] ),
	};

	return (
		<pre { ...props }>
			{ children }
		</pre>
	);
}
