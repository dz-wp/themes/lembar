import img from './img';
import internalLink from './internal-link';
import code from './code';
import pre from './pre';

export default function transform( siteInfo, node, ...args ) {
	switch ( node.tagName ) {
		case 'A':
			return internalLink( siteInfo, node, ...args );

		case 'CODE':
			return code( siteInfo, node, ...args );

		case 'IMG':
			return img( siteInfo, node, ...args );

		case 'PRE':
			return pre( siteInfo, node, ...args );

		default:
			return;
	}
}
