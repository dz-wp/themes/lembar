import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { toggleExternalAsset } from '../../store/actions/external-assets';
import { getPrismComponents, getPrismScriptSrc } from '../../utils/helpers';
import AssetLoader from '../AssetLoader';
import Markup from '../Markup';

export function Prism( props ) {
	const {
		children,
		language,
		onAssetLoaded,
		prismScript,
		...rest
	} = props;

	if ( language === 'none' ) {
		return <code { ...rest }>{ children }</code>;
	}

	return (
		<code { ...rest }>
			{ ! prismScript && (
				<AssetLoader
					data-manual
					id="prismScript"
					src={ getPrismScriptSrc() }
					type="script"
					onLoaded={ () => onAssetLoaded( 'prismScript' ) }
				/>
			) }
			{ typeof window.Prism === 'undefined'
				? children
				: (
					<Markup
						content={ window.Prism.highlight(
							children,
							window.Prism.languages[ language ],
							language
						) }
					/>
				)
			}
		</code>
	);
}

Prism.propTypes = {
	children: PropTypes.string.isRequired,
	prismScript: PropTypes.shape( {
		loaded: PropTypes.bool,
	} ),
	language: PropTypes.oneOf( [
		...getPrismComponents(),
		'none',
	] ).isRequired,
	onAssetLoaded: PropTypes.func.isRequired,
};

function mapStateToProps( state ) {
	const { externalAssets } = state;
	const { prismScript = null } = externalAssets;

	return {
		prismScript,
	};
}

function mapDispatchToProps( dispatch ) {
	return {
		onAssetLoaded: id => dispatch( toggleExternalAsset( id, true ) ),
	};
}

export default connect( mapStateToProps, mapDispatchToProps )( Prism );
