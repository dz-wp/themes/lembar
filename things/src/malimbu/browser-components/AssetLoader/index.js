import { useState } from 'react';
import PropTypes from 'prop-types';

import { injectScript, injectStyle } from '../../utils/helpers';

export default function AssetLoader( props ) {
	const { type, ...rest } = props;
	const [ isPrinted, setIsPrinted ] = useState( false );

	if ( typeof window.navigator === 'undefined' || isPrinted ) {
		return null;
	}

	const found = document.getElementById( rest.id );

	if ( found ) {
		return null;
	}

	if ( type === 'script' ) {
		injectScript( rest );
	} else {
		injectStyle( rest );
	}

	setIsPrinted( true );

	return null;
}

AssetLoader.defaultProps = {
	onLoaded: () => {},
};

AssetLoader.propTypes = {
	id: PropTypes.string.isRequired,
	src: PropTypes.string.isRequired,
	onLoaded: PropTypes.func,
	type: PropTypes.oneOf( [
		'script',
		'style',
	] ).isRequired,
};
