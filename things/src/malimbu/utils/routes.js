import HomeComponent from '../pages/Home';
import SearchComponent from '../pages/Search';
import SingleComponent from '../pages/Single';
import TermComponent from '../pages/Term';

export function createPagedRoutePath( base, prefix = '' ) {
	return `${prefix}/${base}/:page(\\d+):ignored?`;
}

export function createTermRoutes( routes, taxonomies, component ) {
	const { paged } = routes;
	let taxRoutes = [];

	Object.values( taxonomies ).forEach( ( { routePath, rest_base } ) => {
		const extraProps = { taxonomy: rest_base };

		taxRoutes = taxRoutes.concat( [
			{
				component,
				extraProps,
				path: createPagedRoutePath( paged, routePath ),
			},
			{
				component,
				extraProps,
				path: routePath,
			},
		] );
	} );

	return taxRoutes;
}

export default function createRoutes( routes, taxonomies, components = {} ) {
	const { front, paged, search } = routes;
	const { Home, Search, Single, Term } = {
		Home: HomeComponent,
		Search: SearchComponent,
		Single: SingleComponent,
		Term: TermComponent,
		...components,
	};

	return [
		{
			path: '/',
			exact: true,
			component: Home,
		},
		{
			path: createPagedRoutePath( paged, `/${search}/:search` ),
			component: Search,
		},
		{
			path: `/${search}/:search`,
			component: Search,
		},
		...createTermRoutes( routes, taxonomies, Term ),
		{
			path: `${front}:slug/:media`,
			component: Single,
			extraProps: {
				paramsKey: 'media',
				type: 'media',
			},
		},
		{
			path: `${front}:slug`,
			component: Single,
			extraProps: {
				paramsKey: 'slug',
				type: 'posts',
			},
		},
		{
			path: createPagedRoutePath( paged ),
			component: Home,
		},
		{
			path: '/:parentslug/:slug',
			component: Single,
			extraProps: {
				paramsKey: 'slug',
				type: 'pages',
			},
		},
		{
			path: '/:slug([^\\?]+)',
			component: Single,
			extraProps: {
				paramsKey: 'slug',
				type: 'pages',
			},
		},
		{
			path: '*',
			component: Home,
		},
	];
}
