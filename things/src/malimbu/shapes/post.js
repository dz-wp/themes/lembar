import PropTypes from 'prop-types';

export default {
	caption: PropTypes.shape( {
		raw: PropTypes.string,
		rendered: PropTypes.string.isRequired,
	} ),
	content: PropTypes.shape( {
		raw: PropTypes.string,
		rendered: PropTypes.string.isRequired,
	} ),
	description: PropTypes.shape( {
		raw: PropTypes.string,
		rendered: PropTypes.string.isRequired,
	} ),
	date: PropTypes.string.isRequired,
	link: PropTypes.string.isRequired,
	routePath: PropTypes.string.isRequired,
	title: PropTypes.shape( {
		raw: PropTypes.string,
		rendered: PropTypes.string.isRequired,
	} ),
};
