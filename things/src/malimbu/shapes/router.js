import PropTypes from 'prop-types';

export default {
	component: PropTypes.func.isRequired,
	exact: PropTypes.bool,
	path: PropTypes.string.isRequired,
};
