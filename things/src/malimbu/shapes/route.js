import PropTypes from 'prop-types';

export default {
	location: PropTypes.shape( {
		hash: PropTypes.string.isRequired,
		key: PropTypes.string,
		pathname: PropTypes.string.isRequired,
		search: PropTypes.string.isRequired,
		state: PropTypes.string,
	} ),
	match: PropTypes.shape( {
		path: PropTypes.string.isRequired,
		params: PropTypes.objectOf( PropTypes.string ).isRequired,
	} ),
};
