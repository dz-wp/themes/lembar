import PropTypes from 'prop-types';

export default {
	description: PropTypes.string.isRequired,
	hierarchical: PropTypes.bool.isRequired,
	name: PropTypes.string.isRequired,
	rest_base: PropTypes.string.isRequired,
	slug: PropTypes.string.isRequired,
	taxonomies: PropTypes.arrayOf( PropTypes.string ).isRequired,
};
