import PropTypes from 'prop-types';

export default {
	rest_base: PropTypes.string.isRequired,
	routePath: PropTypes.string.isRequired,
	slug: PropTypes.string.isRequired,
};
