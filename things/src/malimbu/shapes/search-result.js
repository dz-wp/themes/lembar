import PropTypes from 'prop-types';

export default {
	id: PropTypes.number.isRequired,
	subtype: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired,
	url: PropTypes.string.isRequired,
};
