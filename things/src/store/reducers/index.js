const dummy = ( state = {} ) => state;

export default function reducers() {
	return {
		misc: dummy,
	};
}
