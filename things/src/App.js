import React from 'react';
import PropTypes from 'prop-types';
import { hot, setConfig } from 'react-hot-loader';
import { __ } from '@wordpress/i18n';
import { Route, Switch } from 'react-router-dom';

import './App.pcss';

import { routerShape } from 'malimbu/shapes';

import { Main, ScreenReaderText, SiteFooter, SiteHeader } from './components';

setConfig( {
	trackTailUpdates: false,
} );

export function App( props ) {
	const { routes } = props;

	return (
		<div className="app">
			<ScreenReaderText TagName="a" href="#site-content">
				{ __( 'Skip to content', 'lembar' ) }
			</ScreenReaderText>
			<ScreenReaderText TagName="a" href="#main-menu">
				{ __( 'Skip to menu', 'lembar' ) }
			</ScreenReaderText>
			<ScreenReaderText TagName="a" href="#search-form">
				{ __( 'Skip to search form', 'lembar' ) }
			</ScreenReaderText>
			<SiteHeader />
			<Main>
				<Switch>
					{ routes.map( ( route, i ) => {
						const { component: Component, extraProps = {}, ...rest } = route;
						const render = routeProps => <Component { ...routeProps } { ...extraProps } />;

						return <Route key={ i } { ...rest } render={ render } />;
					} ) }
				</Switch>
			</Main>
			<SiteFooter />
		</div>
	);
}

App.propTypes = {
	routes: PropTypes.arrayOf(
		PropTypes.shape( routerShape ),
	),
};

export default hot( module )( App );
