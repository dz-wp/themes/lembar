import React from 'react';
import { Provider } from 'react-redux';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';

import init from 'malimbu/utils/init';
import { EnvProvider } from 'malimbu/components';

import App from './App';
import extraReducers from './store/reducers';

const { __lembar_data__, location, print } = global;
const { appRoutes, store } = init( __lembar_data__, { extraReducers } );

const renderedApp = renderToString( (
	<EnvProvider value={ { isServer: true } }>
		<Provider store={ store }>
			<StaticRouter location={ location }>
				<App routes={ appRoutes } />
			</StaticRouter>
		</Provider>
	</EnvProvider>
) );

print( JSON.stringify( renderedApp ) );
